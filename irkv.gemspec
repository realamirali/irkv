Gem::Specification.new do |s|
    s.name        = 'irkv'
    s.version     = '0.2.1'
    s.date        = '2017-04-03'
    s.summary     = ''
    s.description = "A simple key value store"
    s.authors     = ["Amirali Esfandiari"]
    s.email       = 'amiralinull@gmail.com'
    s.files       = ["lib/irkv.rb"]
    s.homepage    =
      'http://gitlab.com/amiralinull/irkv/'
    s.license       = 'GPL-3.0+'
  end
